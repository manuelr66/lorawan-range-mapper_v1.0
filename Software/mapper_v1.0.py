import paho.mqtt.client as mqtt
from influxdb import InfluxDBClient
import json
import dateutil.parser
import time
broker_address = "*********"
# applicationID=164
database="******"
measurement="****************"
devEUI= "**************"
username = "************"
password = "***********"
#devices=[["1234567898765567"],["0097ee23465c51f0"],["304753468704fc1b"],["00bb58c690db39a0"],["00ba7091b08031dc"]]
def on_connect(client, userdata, flags, rc):
  print("Connected with result code {0}".format(str(rc)))  # Print result of connection attempt
  client.subscribe("application/168/device/"+devEUI+"/event/up")

def on_message(client, userdata, msg):
  try:
    output=json.loads(msg.payload.decode())
    # print(output)
    # print("ok")
    data= output['object']
    # print(data)
            
    rxinfo=output['rxInfo']
    # print(rxinfo)
    # print("ok1")

    gatewaysignal = {}

    for i in rxinfo:
      gatewaysignal[i['name']] = i['rssi']

    # print (gatewaysignal)

    error = 0

    for k in data.keys():
      # print (k)
      if k == 'error':
        # print('error')
        error = 1
        break
      else:
        error = 0
        # print('else error')
        break
    
    if error == 0:
      data.update(gatewaysignal)
    #   print(data)
      
      json_body = [
        {
    
        "measurement": measurement,
        "tags":{

        },
        "fields": data
        }
                  ]
    #   print('json')
    #   print(json_body)      
      
    #   print(database)
      time.sleep(1)
      result = influxdbClient.write_points(json_body)
    #   print (result)
      time.sleep(1)
    # print('done')
    
  except:
    pass
client = mqtt.Client("********************")
client.username_pw_set(username, password)
influxdbClient = InfluxDBClient(host='192.168.8.**', port=8086,username='*****', password='***************')
influxdbClient.switch_database(database)
# print(influxdbClient)
client.connect(broker_address, 1883,60)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()  # Start networking daemon
